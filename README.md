# Scar Generator #

This is a personal project using C++ and opencv to apply Harry Potter's scar on faces.
## Prerequisite ##
***
* visual studio (community version), with C++ compiler
* OpenCV
* This solution will only works on windows machine
## Installation ##
***
#### clone ####
`git clone https://fongderek@bitbucket.org/fongderek/scar-generator.git`

#### build ####

1. Run CMake with Visual Studio compiler / generator
2. Run HelloWorld.exe in the build folder with 1 argument (absolute file path of the face image)

#include <iostream>
#include <vector>
#include <string>
#include <windows.h>

#include <opencv2/opencv.hpp>

using namespace std;

// return path to directory of a file path
std::string getDirectory (const std::string& path)
{
    size_t found = path.find_last_of("/\\");
    return(path.substr(0, found));
}

int main(int argc, char** argv)
{
    // check to make sure argument exists for the path to picture
    if (argc < 2) {
        cout << "missing picture path" << endl;
        return 1;
    }

    // find directory where executable is
    char ownPth[MAX_PATH]; 
    HMODULE hModule = GetModuleHandle(NULL);
    if (hModule == NULL) {
        cout << "Module handle is NULL" << endl;
        return 1;
    }
    GetModuleFileName(hModule, ownPth, (sizeof(ownPth)));
    std::string dir = getDirectory(ownPth);

    // load image to draw on
    cv::Mat image = cv::imread(argv[1], cv::IMREAD_COLOR);
    if( image.empty() ) {
        cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }

    // load scar image
    cv::Mat scar = cv::imread(dir + "\\scar.png", cv::IMREAD_UNCHANGED);
    if( scar.empty() ) {
        cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }

    // make sure scar is an bgra image
    if(scar.channels() == 3) {
        cv::Mat dst;
        cv::cvtColor(scar, dst, cv::COLOR_BGR2BGRA, 4);
        scar = dst;
    }

    // setup face and eyes detectors
    cv::CascadeClassifier faceCascade, eyesCascade;
    faceCascade.load(dir + "\\haarcascade_frontalface_alt2.xml");
    eyesCascade.load(dir + "\\haarcascade_eye_tree_eyeglasses.xml");

    // prepare image for face detection
    cv::Mat frame_gray;
    cv::cvtColor( image, frame_gray, cv::COLOR_BGR2GRAY );
    cv::equalizeHist( frame_gray, frame_gray );

    // detect faces in image
    std::vector<cv::Rect> faces;
    faceCascade.detectMultiScale( frame_gray, faces );
    
    // loop through all the faces
    for ( size_t i = 0; i < faces.size(); i++ )
    {
        // draw an oval over the face
        cv::Point center = (faces[i].tl() + faces[i].br()) /2;
        cv::ellipse( image, center, 
            cv::Size( faces[i].width/2, faces[i].height/2 ), 
            0, 0, 360, cv::Scalar( 255, 0, 255 ), 4 );

        // crop out the face from the image
        cv::Mat faceROI = frame_gray( faces[i] );

        // detect eyes in cropped image
        std::vector<cv::Rect> eyes;
        eyesCascade.detectMultiScale( faceROI, eyes );

        // if doesn't find exactly 2 eyes, skip this face
        if(eyes.size() != 2) continue;

        // find left and right eyes
        cv::Rect left_eye = eyes[0].x < eyes[1].x ? eyes[0] : eyes[1];
        cv::Rect right_eye = eyes[1].x < eyes[0].x ? eyes[0] : eyes[1];
        left_eye += faces[i].tl();
        right_eye += faces[i].tl();

        // find the centers of the eyes, and the left to right eye vector
        cv::Point2f left_eye_center = (left_eye.tl() + left_eye.br())/2;
        cv::Point2f right_eye_center = (right_eye.tl() + right_eye.br())/2;
        cv::Point2f left_to_right_eye = right_eye_center - left_eye_center;

        // find the face up vector by rotating left_to_right_eye vector 90 degrees
        cv::Point2f face_y_vec(left_to_right_eye.y, -left_to_right_eye.x);

        // find the center of where the scar will be
        constexpr double scar_offset_factor = 0.625;
        cv::Point2f scar_center = left_eye_center + 
            face_y_vec * scar_offset_factor;

        // find the distance between left and right eyes
        double eyes_distance = cv::norm(left_to_right_eye);

        // normalize the horizontal and vertical vectors
        cv::Point2f normalized_left_to_right_eye = 
            left_to_right_eye / eyes_distance;
        cv::Point2f normalized_face_y_vec = face_y_vec / eyes_distance;

        // find the size of the scar
        constexpr double resize_factor = 0.5;
        double scar_height = eyes_distance * resize_factor;
        double scar_width = scar_height * scar.size().width / scar.size().height;
        cv::Size scar_size(scar_width, scar_height);

        // find the rotated rect of the scar, and its bounding rect
        cv::Point2f rotated_scar_top_right = scar_center + 
            normalized_left_to_right_eye * 0.5 * scar_width + 
            normalized_face_y_vec * 0.5 * scar_height;
        cv::Point2f rotated_scar_top_left = rotated_scar_top_right - 
            normalized_left_to_right_eye * scar_width;
        cv::Point2f rotated_scar_bottom_left = rotated_scar_top_left - 
            normalized_face_y_vec * scar_height;
        cv::RotatedRect rotated_scar_rect(
            rotated_scar_top_right, 
            rotated_scar_top_left, 
            rotated_scar_bottom_left);
        cv::Rect scar_rect = rotated_scar_rect.boundingRect();
        cv::Rect2f scar_rect2f = rotated_scar_rect.boundingRect2f();

        // calculate Affine Transform to resize and rotate original scar image
        cv::Point2f affine_src_pts[] = {
            cv::Point2f(0,0), 
            cv::Point2f(scar.size().width, 0),
            cv::Point2f(0, scar.size().height)
        };
        cv::Point2f affine_dst_pts[] = {
            rotated_scar_top_left - scar_rect2f.tl(), 
            rotated_scar_top_right - scar_rect2f.tl(), 
            rotated_scar_bottom_left - scar_rect2f.tl()
        };
        cv::Mat transform = cv::getAffineTransform(
            affine_src_pts, affine_dst_pts);

        // generate the resized and rotated scar image
        cv::Mat scar_img(scar_rect.size(), scar.type(), cv::Scalar::all(0));
        cv::warpAffine(scar, scar_img, transform, scar_img.size());

        // split scar image into bgr and alpha images
        cv::Mat scar_img_bgr(scar_img.size(), CV_8UC3);
        cv::Mat scar_img_alpha(scar_img.size(), CV_8UC1);
        cv::Mat scar_img_out[] = { scar_img_bgr, scar_img_alpha };
        int from_to[] = { 0,0, 1,1, 2,2, 3,3 };
        cv::mixChannels( &scar_img, 1, scar_img_out, 2, from_to, 4 );

        // generate three channels scar alpha image
        cv::Mat channels[3] = {scar_img_alpha, scar_img_alpha, scar_img_alpha};
        cv::Mat scar_img_alpha_3chn;
        cv::merge(channels, 3, scar_img_alpha_3chn);

        // output = image*(1 - alpha) + scar*alpha
        scar_img_bgr = scar_img_bgr.mul(scar_img_alpha_3chn, 1.0/255.0);
        scar_img_alpha_3chn = cv::Scalar::all(255) - scar_img_alpha_3chn;
        image(scar_rect) = 
            image(scar_rect).mul(scar_img_alpha_3chn, 1.0/255.0) + 
            scar_img_bgr;

        // draw circles over the eyes
        for ( size_t j = 0; j < eyes.size(); j++ )
        {
            cv::Point eye_center( faces[i].x + eyes[j].x + eyes[j].width/2, 
                faces[i].y + eyes[j].y + eyes[j].height/2 );
            int radius = round( (eyes[j].width + eyes[j].height)*0.25 );
            cv::circle( image, eye_center, radius, cv::Scalar( 255, 0, 0 ), 4 );
        }
    }

    cv::namedWindow( "Hello World", cv::WINDOW_AUTOSIZE );

    cv::imshow("Hello World", image);

    cv::waitKey(0);

    return 0;
}